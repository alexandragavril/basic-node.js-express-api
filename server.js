const express = require("express");
const bodyParser = require("body-parser");
const router = require("./routes");
const app = express();

app.use(bodyParser.json());
app.use("/api", router);

const port = 8080;
app.listen(port, () => {
  console.log("Aplicatia ruleaza pe port " + port);
});
