const express = require("express");
const router = express.Router();
const otherRouter = require("./other");
const produseRouter = require("./produse");

router.use("/", otherRouter);
router.use("/", produseRouter);
module.exports = router;
