const express = require("express");
const router = express.Router();
const produse = require("../controllers").produse;

router.post("/addProdus", produse.addProdus);
router.get("/getProduse", produse.getProduse);
router.put("/updatePret/:id", produse.updatePret);
router.delete("/deleteProdus/:id", produse.deleteProdus);
module.exports = router;
