const sequelize = require("../config/db");

module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "produse",
    {
      nume: DataTypes.STRING,
      pret: DataTypes.FLOAT,
      cantitate: DataTypes.INTEGER,
    },
    {
      underscored: true,
      tableName: "produse",
    }
  );
};
