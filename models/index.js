const db = require("../config/db");
const Produse = db.import("./produse");

module.exports = {
  connection: db,
  Produse,
};
