const produse = require("../models/produse");

const produseDB = require("../models").Produse;

const controller = {
  addProdus: async (req, res) => {
    let errors = [];
    const produs = {
      nume: req.body.nume,
      pret: req.body.pret,
      cantitate: req.body.cantitate,
    };

    if (!produs.nume || !produs.pret || !produs.cantitate) {
      errors.push("Avem campuri necompletate!!!");
      console.log("Avem campuri necompletate ba");
    }

    if (errors.length === 0) {
      try {
        await produseDB.create(produs);
        res.status(201).send({ message: "Produs adaugat!" });
      } catch (error) {
        console.log(error);
        res.status(500).send({ message: "EROARE" });
      }
    } else {
      res.status(400).send(errors);
    }
  },

  getProduse: async (req, res) => {
    produseDB
      .findAll({
        attributes: ["nume", "pret", "cantitate"],
      })
      .then((produse) => res.status(200).send(produse))
      .catch(() => {
        res.status(500).send({ message: "Eroare la bd" });
      });
  },

  updatePret: async (req, res) => {
    try {
      const produs = await produseDB.findOne({
        where: {
          id: req.params.id,
        },
      });
      if (produs) {
        if (!req.body.pret) {
          res.status(400).send({ message: "Nu ai introdus un pret nou!!!" });
        } else {
          produs
            .update({
              pret: req.body.pret,
            })
            .then(() => res.status(200).send({ message: "Produs modificat" }));
        }
      }
    } catch {
      res.status(500).send({ message: "Eroare" });
    }
  },

  deleteProdus: async (req, res) => {
    try {
      const produs = await produseDB.findOne({
        where: {
          id: req.params.id,
        },
      });
      if (produs) {
        await produs.destroy();
        res.status(200).send({ message: "Produs distrus" });
      }
    } catch {
      res.status(500).send({ message: "eroare" });
    }
  },
};

module.exports = controller;
