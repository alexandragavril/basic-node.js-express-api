const other = require("./other");
const produse = require("./produse");

const controllers = {
  other,
  produse,
};

module.exports = controllers;
